﻿import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#x(t) = e^σt(cosωt + jsinωt) assume σ == 0 and ω != 0
 
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

t = np.arange(0, 2*np.pi, 0.001) #start value, end value, step between t[n] and t[n-1]
r = 1
C = 1
j =  C*np.exp(r*t)*(1j * np.sin(2*np.pi*1*t))
xs = t
ys = j.imag
zs = C*np.exp(r*t)*np.cos(2*np.pi*1*t) #sin(2πft + φ) -> assume φ = 0 rad
ax.scatter(xs, ys, zs)

ax.set_xlabel('Time')
ax.set_ylabel('Imaginary Axis')
ax.set_zlabel('Real Axis')
plt.suptitle('Real Exp. Signal')

plt.show()
