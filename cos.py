import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

def cos(A, frequency, t, phase):
    return A*np.cos(frequency*t+phase)

def sin(A, frequency, t, phase):
    return A*np.sin(frequency*t+phase)




if __name__ == "__main__":
    t1 = np.arange(-8*np.pi, 8*np.pi, 0.1)

    mpl.style.use('dark_background')


    plt.figure()
    ax1 = plt.subplot(211)
    ax1.axhline(0, color='yellow', lw=0.3)
    ax1.axvline(0, color='yellow', lw=0.3)
    plt.plot(t1, cos(1, np.pi/8, t1, 0), 'k', color = 'greenyellow', lw = 0.8)
    
    ax2 = plt.subplot(212)
    ax2.axhline(0, color='yellow', lw=0.3)
    ax2.axvline(0, color='yellow', lw=0.3)
    plt.plot(t1, sin(1, np.pi/8, t1, 0), 'k', color = 'greenyellow', lw = 0.8)
    plt.show()

