import numpy as np
import matplotlib.pyplot as plt

def x1(t):
    sigma = 2
    return np.exp(sigma*t)
def x2(t):
    sigma = -2
    return np.exp(sigma*t)

t = np.arange(-2.0, 2.0, 0.01)

plt.figure(1)
plt.plot(t, x1(t), t, x2(t))
plt.suptitle('Real Exp. Signal')
plt.xlabel('t')
plt.ylabel('x(t)')
plt.show()
