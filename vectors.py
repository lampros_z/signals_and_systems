import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from math import atan2, sqrt
from matplotlib import animation

def cos(A, frequency, t, phase):
    return A*np.cos(frequency*t+phase)

def sin(A, frequency, t, phase):
    return A*np.sin(frequency*t+phase)

def update_quiver(num, Q, X, Y):
    """updates the horizontal and vertical vector components by a
    fixed increment on each frame
    """

    U = np.cos(X + num*0.01)
    V = np.sin(Y + num*0.01)

    Q.set_UVC(U,V)

    return Q

def update_quiver_negate(num, Q, X, Y):
    """updates the horizontal and vertical vector components by a
    fixed increment on each frame
    """

    U = np.cos(X + num*0.01)
    V = -np.sin(Y + num*0.01)

    Q.set_UVC(U,V)

    return Q

if __name__ == "__main__":
    theta = np.arange(0, 2*np.pi, 0.001)
    mpl.style.use('dark_background')
    fig = plt.figure()
    plt.axhline(0, color='greenyellow', lw=0.5)
    plt.axvline(0, color='greenyellow', lw=0.5)
    x_pos = 0
    y_pos = 0
    plt.plot(cos(1, 1, theta, 0), cos(1, 1, theta, -np.pi/2),'k', color = 'yellow', lw = 4)
    #r = sqrt(x.^2 + y.^2); #% r in function of (x, y)
    #theta = atan2(y, x); # theta in function of (x, y)
    #u = r.*cos(theta); # x component of the vector field
    #v = r.*sin(theta); # y component of the vector field

    X = 0
    Y = 0
    U = 0
    V = 0
    Q = plt.quiver(X, Y, U, V, color='greenyellow', scale_units='xy', scale = 1.0199)
    Q1 = plt.quiver(X, Y, U, V, color='b', scale_units='xy', scale = 1.0199)
    anim = animation.FuncAnimation(fig, update_quiver, fargs=(Q, X, Y), interval=50, blit=False)
    anim1 = animation.FuncAnimation(fig, update_quiver_negate, fargs=(Q1, X, Y), interval=50, blit=False)
    plt.show()