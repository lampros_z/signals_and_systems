import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from math import atan2, sqrt
from matplotlib import animation
from scipy import integrate

def f1(t):
    y = []
    for i in t:
        if (i < 0) :
            y.append(0)
        else:
            y.append(1)
    return np.array(y)

def f2(t):
    y = []
    for i in t:
        if (i < 0) :
            y.append(0)
        else:
            y.append(np.exp(-2*i))
    return np.array(y)

def prod(tau):
    u_tau = f1(tau)
    h_flipped = f2(0.5-tau)
    return np.array([a*b for a,b in zip(u_tau,h_flipped)])

def updateConvolution(num):
    return num+1

def showConvolution(f1, f2, t0):
    # Calculate the overall convolution result using Simpson integration
    convolution = np.zeros(len(t))
    for n, t_ in enumerate(t):
        prod = lambda tau: f1(tau) * f2(t_-tau)
        convolution[n] = integrate.simps(prod(t), t)
    
    # Create the shifted and flipped function
    f_shift = lambda t: f2(t0-t)
    prod = lambda tau: f1(tau) * f2(t0-tau)

    # Plot the curves
    plt.subplot(211)
    plt.plot(t, f1(t), label=r'$f_1(\tau)$')
    plt.plot(t, f_shift(t), label=r'$f_2(t_0-\tau)$')
    plt.plot(t, prod(t), 'r-', label=r'$f_1(\tau)f_2(t_0-\tau)$')
    
    # plot the convolution curve
    plt.subplot(212)
    plt.plot(t, convolution, label='$(f_1*f_2)(t)$')
    # recalculate the value of the convolution integral at the current time-shift t0
    current_value = integrate.simps(prod(t), t)
    plt.plot(t0, current_value, 'ro')  # plot the point
    

if (__name__) == "__main__":
    # plt.plot(t, u(t), label=r'$u(\tau)$')
    # plt.plot(t, h(t0-t), label=r'$h(t_0-\tau)$')
    t = np.arange(-6, 6, 1/50)
    fig = plt.figure()
    showConvolution(f1, f2 , 4)
    plt.show()